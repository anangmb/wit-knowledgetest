const express = require('express')
const router = express.Router()

router.post('/',require('../src/products/create_product').exec)
router.get('/',require('../src/products/get_all_product').exec)
router.get('/count',require('../src/products/count_product').exec)
router.put('/update/:product_id',require('../src/products/update_product').exec)
router.get('/:product_id',require('../src/products/get_product').exec)
router.delete('/:product_id',require('../src/products/delete_product').exec)

module.exports = router