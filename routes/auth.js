const express = require('express')
const router = express.Router()

router.post('/register',require('../src/auth/register').exec)
router.post('/login',require('../src/auth/login').exec)
router.get('/logout',require('../src/auth/logout').exec)
router.get('/sessions',require('../src/auth/sessions').exec)

module.exports = router