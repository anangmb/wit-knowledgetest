const express = require('express')
const app = express()
require('dotenv').config() 
const port = process.env.PORT || 8080

app.use(express.json())
app.use(express.urlencoded())

app.use('/auth',require('./routes/auth'))
app.use('/product',require('./routes/product'))

app.use((req,res,next)=>{
    res.send(404)
})


app.listen(port,()=>{
    console.log(`App listening on http://localhost:${port}`)
})

