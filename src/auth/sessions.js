const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');

const service = {
    protected: true,
    input: async function (input) {
        let data = input.body
        data.query = input.query
        return data
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let sessions = await db('api_keys').where('email',input.auth.email).select('email','created_at')
            return Response.ok(sessions)

        } catch (err) {
            console.log(err)
            if (err.detail) {
                return Response.fail(err.detail, err)
            } else if (err.errorMessage) {
                return Response.fail(err.errorMessage, err.errorList, err.status)
            } else {
                return Response.fail('Unhandled Exception', err)
            }
        }
    },
    validation: {

    }
}

module.exports = Service(service);