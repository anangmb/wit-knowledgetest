const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');
const bcrypt = require('bcryptjs');
require('dotenv').config()
const crypt = require('crypto')
const jwt = require('jsonwebtoken')
const moment = require('moment')

const service = {
    protected: false,
    input: async function (input) {
        return input.body;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            // Get user data from database
            let user = await db('users').where('email',input.email).first('id','email','name','password')
            // Check if user exists
            if(!user){
                throw new CoreException(`User ${input.email} not found!`,[],404)
            }
            
            // Validate user's password
            if(!bcrypt.compareSync(input.password, user.password)){
                throw new CoreException(`Invalid Password!`)

            }

            // Generate random byte as user key
            let key = crypt.randomBytes(32).toString('hex')

            // Store key as user's session
            let store_session = {
                api_key:key,
                email:input.email,
                created_by:user.id,
                updated_by:user.id,
            }
            
            await db('api_keys').insert(store_session)
            
            // Prepare data for jwt payload
            let payload = {
                jti : key,
                uid:user.id,
                email : user.email,
                exp : moment().unix() + (3600*72)
            }

            // Generate and sign jwt
            let token = jwt.sign(payload, process.env.APP_KEY)
            
            // Return token
            return Response.ok({'token':token});
        } catch (err) {
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exceptopn',err)
        }
    },
    validation: {
        email: 'required',
        password: 'required'
    }
}

module.exports = Service(service);