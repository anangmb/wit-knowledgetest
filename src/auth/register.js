const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');
const bcrypt = require('bcryptjs');
require('dotenv').config()

const service = {
    protected: false,
    input: async function (input) {
        return input.body;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let hash = bcrypt.hashSync(input.password)

            let store_user = {
                name: input.name,
                email: input.email,
                password: hash
            }

            let user = await db('users').insert(store_user).returning(['id', 'name', 'email'])

            return Response.ok(user[0],201);
        } catch (err) {
            if(err.detail){
                return Response.fail(err.detail, err)
            }
           return Response.fail(err.errorMessage, err.errorList, err.status)
        }
    },
    validation: {
        name: 'required',
        email: 'required',
        password: 'required'
    }
}

module.exports = Service(service);