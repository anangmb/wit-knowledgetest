const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');

const service = {
    protected: true,
    input: async function (input) {
        
        return input.body;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let store_product = {
                name:input.name,
                price:input.price,
                created_at:"now()",
                updated_at:"now()",
                created_by:input.auth.uid,
                updated_by:input.auth.uid,
            }

            let product = await db('products').insert(store_product).returning(['id','name','price'])

            return Response.ok(product[0],201)
           
        } catch (err) {
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exception',err)
        }
    },
    validation: {
        name:'required|minLength:3',
        price:'required|numeric'
    }
}

module.exports = Service(service);