const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');

const service = {
    protected: true,
    input: async function (input) {
        let data = input.body;
        data.id = input.params.product_id
        return data;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let product = await db('products').where('id',input.id).first()

            if(product == null || product == undefined){
                throw new CoreException(`Product ${input.id} Not Found!`,[],404)
            }
        
            return Response.ok(product)
           
        } catch (err) {
            console.log(err)
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exception',err)
        }
    },
    validation: {
        id:'numeric'
    }
}

module.exports = Service(service);