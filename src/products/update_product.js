const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');

const service = {
    protected: true,
    input: async function (input) {
        let data = input.body;
        data.id = input.params.product_id
        return data;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let product = await db('products').where('id',input.id).first('id')

            if(product == null || product == undefined){
                throw new CoreException(`Product ${input.id} Not Found!`,[],404)
            }

            let to_update = {
                name : input.name,
                updated_by : input.auth.uid,
                updated_at : "now()",
                price : input.price
            }

            let updated_product = await db('products').where('id',input.id).update(to_update).returning(['id','name','price'])

            return Response.ok(updated_product[0])
           
        } catch (err) {
            console.log(err)
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exception',err)
        }
    },
    validation: {
        id:'numeric',
        name:'required|minLength:3',
        price:'required|numeric'
    }
}

module.exports = Service(service);