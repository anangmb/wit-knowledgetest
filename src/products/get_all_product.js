const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');


const service = {
    protected: true,
    input: async function (input) {
        return input.query;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let limit = 10
            let offset = 0
            let current_page = 1

            if(input.item_per_page != null && input.item_per_page != undefined){
                limit = input.item_per_page
            }

            if(input.page != null && input.page != undefined && input.page > 1){
                current_page = input.page
                offset = (input.page-1) * limit
            }

            let query = `TRUE `

            if(input.search != null && input.search != undefined){
                query = query + `AND LOWER(name) LIKE LOWER('%${input.search}%')`
            }

            let products = await db('products').orderBy('id','asc').whereRaw(query).limit(limit).offset(offset)
            let query_count = await db('products').whereRaw(query).count('id')
            query_count = query_count[0].count
        
            return Response.ok({
                'total_item':query_count,
                'total_page':(query_count%limit),
                'current_page':current_page,
                'products':products

            })
           
        } catch (err) {
            console.log(err)
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exception',err)
        }
    },
    validation: {
   
    }
}

module.exports = Service(service);