const db = require('../../utils/database');
const { Service, Response, CoreException } = require('../../utils/service_driver');

const service = {
    protected: true,
    input: async function (input) {
        return input.body;
    },
    prepare: async function (input) {
        return input;
    },
    proccess: async function (input) {
        try {
            let product = await db('products').whereRaw(`price > 80000`).count('id').first();   
            
            return Response.ok(product)
           
        } catch (err) {
            console.log('ERR' + err)
            if(err.detail){
                return Response.fail(err.detail, err)
            }else if(err.errorMessage){
                return Response.fail(err.errorMessage, err.errorList, err.errorCode)
            }
            return Response.fail('Undhandled Exception',err)
        }
    },
    validation: {
   
    }
}

module.exports = Service(service);