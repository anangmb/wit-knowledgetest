const moment = require('moment')
const { Validator } = require('node-input-validator');
const jwt = require('jsonwebtoken');
const db = require('./database');
require('dotenv').config()

class CoreException {
    constructor(errorMessage = "", errorList = {}, errorCode = 422) {
        this.errorMessage = errorMessage;
        this.errorList = errorList;
        this.errorCode = errorCode;
    }
}

const Response = {
    ok: function (data, status = 200) {
        var body = {
            success: true,
            data: data
        }
        var response = {
            status: status,
            resp: body
        }
        return response;
    },
    fail: function (message = "Error", errorList = {}, code = 422) {
        var body = {
            success: false,
            error_code: code,
            message: message,
            error_list: errorList,
        }
        var response = {
            status: code,
            resp: body
        }
        return response;
    }
}

const Auth = async function(req){
    try {
        if(req.headers.authorization == null || req.headers.authorization == undefined){
            throw new CoreException('Token required')
        }
        let token = req.headers.authorization.split(" ")[1];
        let decoded = jwt.verify(token, process.env.APP_KEY);
        let api_key = await db('api_keys').whereRaw(`email = ? AND api_key=?`,[decoded.email, decoded.jti]).first('email')
        if(api_key == null){
            throw new CoreException('Token revoked')
        }
        return decoded;
    } catch (err) {
        if(err.message){
            throw new CoreException(err.message);
        }
        throw new CoreException(err.errorMessage);
    }
}

const Service = function (service) {
    return {
        exec: async function (req, res, next) {
            try {
                // CHECK IF PROTECTED
                if(service.protected){
                    var auth_data = await Auth(req)
                }

                // INPUT
                var input = await service.input(req).catch(next);
                if (input === null || input === undefined) {
                    throw new CoreException('Unhandled Input Exception')
                }

                // VALIDATE
                const validator = new Validator(input, service.validation);

                const matched = await validator.check();

                if (!matched) {
                    throw new CoreException('Invalid input',validator.errors,422)
                }

                // PREPARE INPUT
                var preparedInput = await service.prepare(input).catch(next);
                if(service.protected){
                    preparedInput.auth = auth_data
                }
                if (preparedInput === null || preparedInput === undefined) {
                    return Response.fail("Unhandled preparation exception")
                }
                preparedInput.session = {
                    datetime: moment().format('YYYYMMDDHHmmss')
                }

                // MAIN PROCESS
                var result = await service.proccess(preparedInput).catch(next);
                if (result === null || result === undefined) {
                    return Response.fail("Unhandled main process exception")
                }
                res.statusCode = result.status
                return res.json(result.resp);
            } catch (err) {
                let result = Response.fail(err.errorMessage, err.errorList, err.errorCode)
                res.statusCode = result.status
                res.json(result.resp)
            }
        }
    }
}
module.exports = { Response, Service, CoreException }