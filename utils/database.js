require('dotenv').config()

const db = require('knex')({
    client:process.env.DB_DRIVER,
    connection:{
        host:process.env.DB_HOST,
        port:process.env.DB_PORT,
        user:process.env.DB_USER,
        password:process.env.DB_PASSWORD,
        database:process.env.DB_DATABASE
    }
})

db.sql = function(sql,params = []){
    return db.raw(sql,params)
        .then(res=>{
            return res
        })
        .catch(err=>{
            return err
        })
}

module.exports = db