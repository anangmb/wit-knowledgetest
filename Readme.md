# Knowledge Test Backend Developer
Knowledge Test Backend Developer Widya Imersive Teknologi.

This repository contains answer for assginment number
 - 2. Password Validation
 - 5. CRUD Product Table
 - 6. Count product with price > 80000

Documentation for this API available at https://documenter.getpostman.com/view/8973512/UVC2Gp96

---
### How to Run

- clone this repository
- create database in your local environment (tested in postgresql)
- adjust database settings in .env (for app runtime) and database.json (for migration)
- run `npm install`
- run `db-migrate up`
- run `npm start`