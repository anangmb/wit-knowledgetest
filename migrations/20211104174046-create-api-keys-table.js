'use strict';

const { now } = require("moment");

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('api_keys',{
    api_key:'string',
    email:'string',
    created_at:{
      type:'timestamp',
      defaultValue:'now()'
    },
    updated_at:{
      type:'timestamp',
      defaultValue:'now()'
    },
    created_by:'bigint',
    updated_by:'bigint'
  });
};

exports.down = function(db) {
  return db.dropTable('api_keys');
};

exports._meta = {
  "version": 1
};
