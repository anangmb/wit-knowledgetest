'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('products',{
    id:{
      type:'bigint',
      primaryKey:'true',
      autoIncrement:'true'
    },
    name:'string',
    price:'bigint',
    created_at:'timestamp',
    updated_at:'timestamp',
    created_by:'bigint',
    updated_by:'bigint'
  });
};

exports.down = function(db) {
  return db.dropTable('products');
};

exports._meta = {
  "version": 1
};
